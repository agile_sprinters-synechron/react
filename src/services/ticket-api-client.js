const url = process.env.REACT_APP_TICKETS_API_URL;


const ticketAPIClient = {
    getAllTickets: function () {
        var promise = new Promise((resolve, reject) => {
            return fetch(url).then((res) => {
                var result = res.json();
                result.then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("JSON Parse Error");
                });
            }).catch((err) => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

    insertTicket: function (t) {

        const prop1 = 'id';
        const prop2 = 'customerid';
        const newticket = Object.keys(t).reduce((object, key) => {
         if (key !== prop1 && key !== prop2) {
             object[key] = t[key]
            }
                    return object
            }, {})

        const request = new Request(url, {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(newticket)
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("JSON Parse Error");
                })
            }).catch(error => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

  
}

export default ticketAPIClient;