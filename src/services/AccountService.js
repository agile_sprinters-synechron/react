import axios from 'axios'

const GET_PLAYER_REST_API_URL = "http://35.188.88.103:8081/api/accounts/";
const MAKE_TRANSACTION_URL    = "http://35.188.88.103:8081/api/accounts/"
const GET_STATEMENTS_API_URL  = "http://35.188.88.103:8081/api/accounts/"

class AccountService
{
    getBalance(id)
    {
       return axios.get(GET_PLAYER_REST_API_URL+id);
    }

    makeTransaction(transaction)
    {
        return axios.post(MAKE_TRANSACTION_URL+transaction.id,transaction)
    }

    getStatements(id,startdate,enddate)
    {
        return axios.get(GET_STATEMENTS_API_URL+id+"/statement/"+startdate+"/"+enddate);
    }

}

export default new AccountService();