const url = process.env.REACT_APP_LOANS_API_URL;

const loanAPIClient = {
    getAllLoans: function () {
        var promise = new Promise((resolve, reject) => {
            return fetch(url + "/loans").then((res) => {
                var result = res.json();
                result.then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("JSON Parse Error");
                });
            }).catch((err) => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

    insertLoan: function (p) {
        const request = new Request(url + "/loans", {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
            }),
            
            body: JSON.stringify(p)
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("JSON Parse Error");
                })
            }).catch(error => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

    updateStatus: function (p) {
        const request = new Request(url + "/loans", {
            method: 'PUT',
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(p)
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("JSON Parse Error");
                })
            }).catch(error => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

    payLoan: function (p) {
        const request = new Request(url + "/customer/" + p.customerId + "/loans/" + p.loanId, {
            method: 'PUT',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
            }),
            body: JSON.stringify(p)
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("JSON Parse Error");
                })
            }).catch(error => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

    deleteLoan: function (p) {
        const request = new Request(url + "/" + p.loanId, {
            method: 'DELETE'
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then(() => {
                    resolve("Record Deleted");
                }, (err) => {
                    reject("JSON Parse Error");
                })
            }).catch(error => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    }
}

export default loanAPIClient;