import { lazy, Suspense } from "react";
import { Route, Switch, Link } from "react-router-dom";

// Eager Loading
import HomeComponent from "../components/home/HomeComponent";
import LoaderAnimation from '../components/common/LoaderAnimation';
import login from '../components/login/login';


// Lazy Loading (On-Demnad Loading)
const TicketsContainer = lazy(() => import("../containers/tickets/TicketsConatiner"));
const ManageTicketContainer = lazy(() => import("../containers/tickets/ManageTicketContainer"));
const AccountsContainer = lazy(() => import("../containers/accounts/AccountsContainer"));
const TransactionComponent = lazy(() => import("../containers/accounts/TransactionComponent"));
const BalanceComponent = lazy(() => import("../containers/accounts/BalanceComponent"));
const StatementComponent = lazy(() => import("../containers/accounts/StatementComponent"));
const LoanContainer = lazy(() => import("../containers/loans/LoanContainer"));
const ManageLoanContainer = lazy(() => import("../containers/loans/ManageLoanContainer"));
const PayLoanContainer = lazy(() => import("../containers/loans/PayLoanContainer"));
const StatusLoanContainer = lazy(() => import("../containers/loans/StatusLoanContainer"));
const CustomerLoansContainer = lazy(() => import("../containers/loans/CustomerLoansContainer"));

const img404 = require('../assets/http-404.jpg');

export default (
    <Suspense fallback={<LoaderAnimation />}>
        <Switch>
            <Route exact path="/" component={HomeComponent} />
            <Route path="/tickets" component={TicketsContainer} />
            <Route path="/ticket/:id" component={ManageTicketContainer} />
            <Route path="/ticket" component={ManageTicketContainer} />
            <Route path="/login" component={login} />
            <Route path="/accounts" component={AccountsContainer} />
            <Route path="/transaction" component={TransactionComponent} />
            <Route path="/balance" component={BalanceComponent} />
            <Route path="/loans" component={LoanContainer} />
            <Route path="/loan/:loanId" component={ManageLoanContainer} />
            <Route path="/loan" component={ManageLoanContainer} />
            <Route path="/payLoan/:loanId" component={PayLoanContainer} />
            <Route path="/manageLoan/:loanId" component={StatusLoanContainer} />
            <Route path="/viewCustomerLoans" component={CustomerLoansContainer} />
            <Route path="/statements" component={StatementComponent} />

            <Route path="**" render={
                () => (
                    <div className="text-center">
                        <article>
                            <h1 className="text-danger">No Route Configured!</h1>
                            <h4 className="text-danger">Please check your Route Configuration</h4>
                        </article>
                        <div className="mt-5">
                            <img src={img404} alt="Not Found" className="rounded" />
                        </div>
                        <h2 className="mt-5">
                            <Link className="nav-link" to="/">Back to Home</Link>
                        </h2>
                    </div>
                )
            } />
        </Switch>
    </Suspense>
);