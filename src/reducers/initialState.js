const initialState = {
    ticketsData: { tickets: [], status: "", flag: false },
    loansData: { loans: [], status: "", flag: false },
    customerData: { selectOptions: [], name: "", id:"", flag: false }
};

export default initialState;