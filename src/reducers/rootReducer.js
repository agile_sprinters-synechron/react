import { combineReducers } from 'redux';

import ticketReducer from './ticketReducer';
import loanReducer from './loanReducer';

const rootReducer = combineReducers({
     ticketReducer, loanReducer
});

export default rootReducer;