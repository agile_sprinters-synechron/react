// Reducer is a Pure Function
// A reducer is a function that determines changes to an application's state.
// It uses the action it receives via dispatcher, to determine this change. 

import * as actionTypes from '../actions/actionTypes';
import initialState from "./initialState";

const loanReducer = (state = initialState.loansData, action) => {
    switch (action.type) {
        case actionTypes.LOAD_LOANS_REQUESTED:
        case actionTypes.LOAD_LOANS_FAILED:
            return {
                loans: [...state.loans],
                status: action.payload.message,
                flag: action.payload.flag
            };
        case actionTypes.LOAD_LOANS_SUCCESS:
            return {
                loans: [...action.payload.data],
                status: action.payload.message,
                flag: action.payload.flag
            };
        default:
            return state;
    }
}

export default loanReducer;