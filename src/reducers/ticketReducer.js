// Reducer is a Pure Function
// A reducer is a function that determines changes to an application's state.
// It uses the action it receives via dispatcher, to determine this change. 

import * as actionTypes from '../actions/actionTypes';
import initialState from "./initialState";

const ticketReducer = (state = initialState.ticketsData, action) => {
    switch (action.type) {
        case actionTypes.LOAD_TICKETS_REQUESTED:
        case actionTypes.LOAD_TICKETS_FAILED:
            return {
                tickets: [...state.tickets],
                status: action.payload.message,
                flag: action.payload.flag
            };
        case actionTypes.LOAD_TICKETS_SUCCESS:
            return {
                tickets: [...action.payload.data],
                status: action.payload.message,
                flag: action.payload.flag
            };
        case actionTypes.INSERT_TICKET_SUCCESS:
            return {
                tickets: [...state.tickets, { ...action.payload.data }],
                status: action.payload.message,
                flag: action.payload.flag
            };
        default:
            return state;
    }
}

export default ticketReducer;