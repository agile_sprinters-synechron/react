import React from 'react';
import { useState } from 'react';
import { Link } from 'react-router-dom';

import ConfirmModal from '../common/ConfirmModal';

const LoansListComponent = ({ loans, onDelete }) => {
    return (
        <table className="table table-hover">
            <thead>
                <tr>
                    <th>Loan ID</th>
                    <th>Account ID</th>
                    <th>Customer ID</th>
                    <th>Loan Amount</th>
                    <th>Balance Amount</th>
                    <th>Balance Tenure</th>
                    <th>Loan Tenure</th>
                    <th>Status</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                {
                    loans.map(loan => <LoanListRow key={loan.loanId} loan={loan} onDelete={onDelete} />)
                }
            </tbody>
        </table>
    );
};

const LoanListRow = ({ loan, onDelete }) => {
    var [show, setShow] = useState(false);

    return (
        <>
            <tr key={loan.loanId}>
                <td>{loan.loanId}</td>
                <td>{loan.accountId}</td>
                <td>{loan.customerId}</td>
                <td>{loan.loanAmount}</td>
                <td>{loan.balanceAmount}</td>
                <td>{loan.balanceTenure}</td>
                <td>{loan.loanTenure}</td>
                <td>{loan.status}</td>
                <td>
                    <Link className="text-info" to={"manageLoan/" + loan.loanId}>Manage Loan</Link>
                </td>
                <td>
                    <Link className="text-info" to={"payLoan/" + loan.loanId}>Pay Loan</Link>
                </td>
            </tr>

            <ConfirmModal show={show} handleYes={e => {
                onDelete(loan, e);
                setShow(false);
            }} handleNo={e => {
                setShow(false);
            }} />
        </>
    );
};

export default LoansListComponent;