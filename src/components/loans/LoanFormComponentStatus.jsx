import React from 'react';
import { Link } from 'react-router-dom';
import TextInput from '../common/TextInput';

const LoanFormComponentStatus = ({ pageText, loan, onChange, onSave }) => {
    return (
        <>
            <h1 className="text-info text-center">{pageText}</h1>
            <div className="text-center">
                <Link to="/loans">Back to Loans</Link>
            </div>

            <div className="row">
                <div className="col-sm-6 offset-sm-3">
                    <form className="justify-content-center" onSubmit={onSave}>
                        <fieldset>
                            <legend className="text-center">Enter Loan Information</legend>

                            <TextInput name="accountId" label="Account ID" value={loan.accountId} onChange={onChange} />
                            {/* <TextInput name="customerId" label="Customer ID" value={loan.customerId} onChange={onChange} /> */}
                            <TextInput name="loanId" label="Loan ID" value={loan.loanId} onChange={onChange} />
                            <TextInput name="status" label="Loan Status" value={loan.status} onChange={onChange} /> 

                            <div className="d-grid gap-2 mx-auto col mt-3">
                                <button type='submit' className="btn btn-success">Approve Loan</button>
                                <button type='button' className="btn btn-secondary">Reset</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </>
    );
};

export default LoanFormComponentStatus;