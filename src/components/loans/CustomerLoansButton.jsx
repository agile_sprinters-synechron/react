import React from 'react';
import { withRouter } from 'react-router-dom';

const CustomerLoansButton = ({ history }) => {
    return (
        <button className='btn btn-primary' onClick={
            (e) => {
                history.push('/viewCustomerLoans');
            }
        }>
            View Customer Loans
        </button>
    );
};

export default withRouter(CustomerLoansButton);