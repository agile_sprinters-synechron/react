import React from 'react';
import { withRouter } from 'react-router-dom';

const ApplyLoanButton = ({ history }) => {
    return (
        <button className='btn btn-primary' onClick={
            (e) => {
                history.push('/loan');
            }
        }>
            Apply Loan
        </button>
    );
};

export default withRouter(ApplyLoanButton);