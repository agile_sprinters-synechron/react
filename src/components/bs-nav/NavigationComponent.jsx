import React from 'react';
import { NavLink } from 'react-router-dom';

import SwitchComponent from '../../routes';

import './NavigationComponent.css';

var logo = require('../../assets/State_bank_of_mysore.png');

const NavigationComponent = () => {
    return (
        <>
            <nav className="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
                <div className="container-fluid">
                    <NavLink className="navbar-brand d-flex flex-column align-items-center" to="/">
                        <img src={logo} alt="React" width="300" height="80" className="d-inline-block align-text-top" />
                    </NavLink>

                    <button type="button" className="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#myNavbar">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="myNavbar">
                        <ul className="navbar-nav ms-auto">
                            <li className="nav-item px-3">
                                <NavLink exact className="nav-link d-flex flex-column align-items-center" to="/">
                                    <i className="bi bi-house-fill"></i>
                                    <span>Home</span>
                                </NavLink>
                            </li>
                            <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/accounts">
                                    <i className="bi bi-file-person-fill"></i>
                                    <span>Accounts</span>
                                </NavLink>
                            </li>
                            <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/loans">
                                    <i className="bi bi-dice-1-fill"></i>
                                    <span>Loans</span>
                                </NavLink>
                            </li>
                            <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/tickets">
                                    <i className="bi bi-dice-2-fill"></i>
                                    <span>Tickets</span>
                                </NavLink>
                            </li>
                            {/* <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/login">
                                    <i className="bi bi-dice-2-fill"></i>
                                    <span>login</span>
                                </NavLink>
                            </li> */}
                           

                        </ul>
                    </div>
                </div>
            </nav>

            <>
                {SwitchComponent}
            </>
        </>
    );
};

export default NavigationComponent;