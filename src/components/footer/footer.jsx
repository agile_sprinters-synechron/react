import React from "react";
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faYoutube,
  faFacebook,
  faTwitter,
  faInstagram
} from "@fortawesome/free-brands-svg-icons";


import './footer.css';

const FooterPage = () => {
  return (
    <MDBFooter id="footer"  className="font-small pt-4 mt-4 bg-dark text-muted">
      <MDBContainer fluid className="text-center text-md-left">
        <MDBRow>
          <MDBCol md="6">
            <h5 className="title">About us</h5>
            <p>
            State Bank of Mysore was a Public Sector bank in India, with headquarters at Bangalore. It was one of the five associate banks of State Bank of India, all of which were consolidated with the State Bank of India with effect from 1 April 2017.
            </p>
          </MDBCol>
          <MDBCol md="6">
            <h5 className="title">Contact us</h5>
            <a href="https://www.youtube.com/"
  className="youtube social">
  <FontAwesomeIcon icon={faYoutube} size="2x" />
</a>
<a href="https://www.facebook.com/"
  className="facebook social">
  <FontAwesomeIcon icon={faFacebook} size="2x" />
</a>
<a href="https://www.twitter.com/" className="twitter social">
  <FontAwesomeIcon icon={faTwitter} size="2x" />
</a>
<a href="https://www.instagram.com/"
  className="instagram social">
  <FontAwesomeIcon icon={faInstagram} size="2x" />
</a>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
      <div className="footer-copyright text-center py-3">
        <MDBContainer fluid>
          &copy; {new Date().getFullYear()} Copyright: <a href="https://www.mysorebank.com"> mysorebank.com </a>
        </MDBContainer>
      </div>
    </MDBFooter>
  );
}

export default FooterPage;