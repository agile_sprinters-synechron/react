/* eslint-disable */
import React from 'react';
import { Router } from 'react-router-dom';

import ErrorHandler from '../common/ErrorHandler';
import NavigationComponent from '../bs-nav/NavigationComponent';
import history from '../../utilities/history';
import FooterPage from '../footer/footer';

const RootComponent = () => {
    return (
        <div className='container'>
            <ErrorHandler>
                <Router history={history}>
                    <NavigationComponent />
                    <FooterPage/>
                </Router>
            </ErrorHandler>
       
        </div>
       
    );
};

export default RootComponent;