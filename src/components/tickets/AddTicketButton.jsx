import React from 'react';
import { withRouter } from 'react-router-dom';

const AddTicketButton = ({ history }) => {
    return (
        <button className='btn btn-primary' onClick={
            (e) => {
                history.push('/ticket');
            }
        }>
            Create Ticket
        </button>
    );
};

export default withRouter(AddTicketButton);