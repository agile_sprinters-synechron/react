import React from 'react';
import { Link } from 'react-router-dom';

import TextInput from '../common/TextInput';

const TicketFormComponent = ({ pageText, ticket, onChange, onSave }) => {

    return (
        <>
        
            <h1 className="text-info text-center">{pageText}</h1>
            <div className="text-center">
                <Link to="/tickets">Back to List</Link>
            </div>

            <div className="row">
                <div className="col-sm-6 offset-sm-3">
                    <form className="justify-content-center" onSubmit={onSave}>
                        <fieldset>
                            <legend className="text-center">Enter Ticket Information</legend>
                            {/* <TextInput name="customerid" label="customerid" value={ticket.customerid}  onChange={onChange}  /> */}
                            <TextInput type="email" name="email_address" label="email_address" value={ticket.email_address} onChange={onChange}  />
                            <TextInput name="message" label="message" value={ticket.message} onChange={onChange}  />
                            <TextInput name="phone" label="phone" value={ticket.phone} onChange={onChange}  />
                            <label htmlFor="status">Status:</label>
                            <select className="form-control" name="status" label="status" value={ticket.status} onChange={onChange} required>
                               <option value="">None</option>
                                <option value="NEW">NEW</option>
                                <option value="IN-PROCESS">IN-PROCESS</option>
                                <option value="RESOLVED">RESOLVED</option>
                                <option value="CLOSED">CLOSED</option>
                            </select>

                            <div className="d-grid gap-2 mx-auto col mt-3">
                                <button type='submit' className="btn btn-success">Save</button>
                                <button type='button' className="btn btn-secondary">Reset</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </>
    );
};

export default TicketFormComponent;