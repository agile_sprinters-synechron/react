import React from 'react';
//import { useState } from 'react';



const TicketListComponent = ({ tickets, onDelete }) => {
    if (tickets.length === 0)
    {
        return (
            <p className='text-danger'>No tickets present</p>
        );
    }
   else{
    return (
       <div>
        {/* <select className="form-control" name="ticketid">
        <option name="ticketoption" value="All">ALL</option>
            {tickets.map((ticket) => (
              <option value={ticket.id}>{ticket.id}</option>
            ))}
          </select> */}
        <table className="table table-hover">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Customer ID</th>
                    <th>Email</th>
                    <th>Message</th>
                    <th>Phone</th>
                    <th>Status</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                {  
                    tickets.map(ticket => <TicketListRow key={ticket.id} ticket={ticket} />)
                }
            </tbody>
        </table>
        </div>
    );}
};

const TicketListRow = ({ ticket, onDelete }) => {
  //  var [show, setShow] = useState(false);

    return (
        <>
            <tr key={ticket.id}>
                <td>{ticket.id}</td>
                <td>{ticket.customerid}</td>
                <td>{ticket.email_address}</td>
                <td>{ticket.message}</td>
                <td>{ticket.phone}</td>
                <td>{ticket.status}</td>
            </tr>

        </>
    );
};

export default TicketListComponent;