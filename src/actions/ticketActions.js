import * as actionTypes from './actionTypes';

import ticketAPIClient from '../services/ticket-api-client';
import history from '../utilities/history';

function loadTicketsRequested(msg) {
    return {
        type: actionTypes.LOAD_TICKETS_REQUESTED,
        payload: { message: msg, flag: false }
    };
}

function loadTicketsSuccess(Tickets, msg) {
    return {
        type: actionTypes.LOAD_TICKETS_SUCCESS,
        payload: { data: Tickets, message: msg, flag: true }
    };
}

function loadTicketsFailed(msg) {
    return {
        type: actionTypes.LOAD_TICKETS_FAILED,
        payload: { message: msg, flag: true }
    };
}

export function loadTickets() {
    return function (dispatch) {
        dispatch(loadTicketsRequested("Tickets Request Started..."));

        ticketAPIClient.getAllTickets().then(function (tickets) {
                setTimeout(() => {
                    dispatch(loadTicketsSuccess(tickets, "Tickets Request Completed..."));
                }, 3000);
            }).catch(eMsg => {
            dispatch(loadTicketsFailed(eMsg));
        });
    }
}

// ----------------------------------------------------------- INSERT

function insertTicketsuccess(ticket, msg) {
    return {
        type: actionTypes.INSERT_TICKET_SUCCESS,
        payload: { data: ticket, message: msg, flag: true }
    };
}

export function insertTicket(ticket) {
    return function (dispatch) {
        ticketAPIClient.insertTicket(ticket).then(insertTicket => {
            dispatch(insertTicketsuccess(insertTicket, "Ticket Inserted Successfully..."));
            history.push('/Tickets');
        }).catch(eMsg => {
            console.error(eMsg);
        });
    }
}



