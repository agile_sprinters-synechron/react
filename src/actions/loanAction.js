import * as actionTypes from './actionTypes';

import loanAPIClient from '../services/loan-api-client';
import history from '../utilities/history';

function loadLoansRequested(msg) {
    return {
        type: actionTypes.LOAD_LOANS_REQUESTED,
        payload: { message: msg, flag: false }
    };
}

function loadLoansSuccess(loans, msg) {
    return {
        type: actionTypes.LOAD_LOANS_SUCCESS,
        payload: { data: loans, message: msg, flag: true }
    };
}

function loadLoansFailed(msg) {
    return {
        type: actionTypes.LOAD_LOANS_FAILED,
        payload: { message: msg, flag: true }
    };
}

export function loadLoans() {
    return function (dispatch) {
        dispatch(loadLoansRequested("loans Request Started..."));

        loanAPIClient.getAllLoans().then(loans => {
            setTimeout(() => {
                dispatch(loadLoansSuccess(loans, "loans Request Completed..."));
            }, 1000);
        }).catch(eMsg => {
            dispatch(loadLoansFailed(eMsg));
        });
    }
}


// ----------------------------------------------------------- INSERT

function insertLoanSuccess(Loan, msg) {
    return {
        type: actionTypes.INSERT_LOAN_SUCCESS,
        payload: { data: Loan, message: msg, flag: true }
    };
}

export function insertLoan(loan) {
    return function (dispatch) {
        loanAPIClient.insertLoan(loan).then(insertedLoan => {
            dispatch(insertLoanSuccess(insertedLoan, "Loan Inserted Successfully..."));
            history.push('/loans');
        }).catch(eMsg => {
            console.error(eMsg);
        });
    }
}

// ----------------------------------------------------------- UPDATE

function updateLoanSuccess(loan, msg) {
    return {
        type: actionTypes.UPDATE_LOAN_SUCCESS,
        payload: { data: loan, message: msg, flag: true }
    };
}

export function payLoan(loan) {
    return function (dispatch) {
        loanAPIClient.payLoan(loan).then(updatedLoan => {
            dispatch(updateLoanSuccess(updatedLoan, "Loan Paid Successfully..."));
            history.push('/loans');
        }).catch(eMsg => {
            console.error(eMsg);
        });
    }
}


export function updateStatus(loan) {
    return function (dispatch) {
        loanAPIClient.updateStatus(loan).then(updatedLoan => {
            dispatch(updateLoanSuccess(updatedLoan, "Status Updated Successfully..."));
            history.push('/loans');
        }).catch(eMsg => {
            console.error(eMsg);
        });
    }
}

// ----------------------------------------------------------- DELETE

function deleteLoanSuccess(loan, msg) {
    return {
        type: actionTypes.DELETE_LOAN_SUCCESS,
        payload: { data: loan, message: msg, flag: true }
    };
}

export function deleteLoan(loan) {
    return function (dispatch) {
        loanAPIClient.deleteLoan(loan).then(_ => {
            dispatch(deleteLoanSuccess(loan, "Loan Deleted Successfully..."));
        }).catch(eMsg => {
            console.error(eMsg);
        });
    }
}