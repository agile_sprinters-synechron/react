import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as loanAction from '../../actions/loanAction';
import LoaderAnimation from '../../components/common/LoaderAnimation';
import ApplyLoanButton from '../../components/loans/ApplyLoanButton';
import CustomerLoansButton from '../../components/loans/CustomerLoansButton';
import LoanListComponent from '../../components/loans/LoanListComponent';

class LoanContainer extends Component {
    render() {
        return (
            <>
                {
                    this.props.flag ?
                        <>
                            <div className='mt-3 mb-3'>
                                <ApplyLoanButton />&nbsp;&nbsp;
                                <CustomerLoansButton />
                            </div>
                             <LoanListComponent loans={this.props.loans} onDelete={
                                (p, e) => {
                                    this.props.deleteLoan(p);
                                }
                            } /> 
                        </>
                        : <LoaderAnimation />
                }
            </>
        );
    }

    componentDidMount() {
        this.props.loadLoans();
    }
}

function mapStateToProps(state, ownProps) {
    return {
        loans: state.loanReducer.loans,
        status: state.loanReducer.status,
        flag: state.loanReducer.flag
    };
}

function mapDispatchToProps(dispatch) {
    return {
        loadLoans: () => { dispatch(loanAction.loadLoans()); },
        deleteLoan: (loan) => { dispatch(loanAction.deleteLoan(loan)); }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoanContainer);