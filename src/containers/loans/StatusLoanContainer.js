import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoanFormComponentStatus from '../../components/loans/LoanFormComponentStatus';
import getLoanById from '../../selectors/getLoanById';

import * as loanAction from '../../actions/loanAction';

class StatusLoanContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loan: { ...this.props.loan }
        };
        this.updateState = this.updateState.bind(this);
        this.saveLoan = this.saveLoan.bind(this);
    }

    updateState(e) {
        const field = e.target.name;
        let loan = { ...this.state.loan };
        loan[field] = e.target.value;
        this.setState({ loan: loan });
    }

    saveLoan(e) {
        e.preventDefault();
        this.props.updateStatus(this.state.loan);
    }

    render() {
        return (
            <div>
                <LoanFormComponentStatus pageText={this.props.pText} loan={this.state.loan}
                    onChange={this.updateState} onSave={this.saveLoan} />
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    const loanId = ownProps.match.params.loanId;

    let loan = {
        loanId: "",
        accountId: "",
        customerId: "",
        loanAmount: "",
        balanceAmount: "",
        balanceTenure: "",
        loanTenure: "",
        amount: "",
        status: ""
    };

    if (loanId && state.loanReducer.loans.length > 0) {
        loan = getLoanById(state, ownProps);
    }

    var pText = loan.loanId === "" ? "Create loan" : "Edit loan";

    return {
        pText, loan
    };
}

function mapDispatchToProps(dispatch) {
    return {
        updateStatus: (loan) => { dispatch(loanAction.updateStatus(loan)); },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(StatusLoanContainer);