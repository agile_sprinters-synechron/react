import React, { Component } from 'react';
import { connect } from 'react-redux';

import LoaderAnimation from '../../components/common/LoaderAnimation';
import ApplyLoanButton from '../../components/loans/ApplyLoanButton';
import CustomerLoansButton from '../../components/loans/CustomerLoansButton';
import LoanListComponentCustomerLoans from '../../components/loans/LoanListComponentCustomerLoans';
import Select from 'react-select';
import axios from 'axios';

import * as loanAction from '../../actions/loanAction';

class CustomerLoansContainer extends Component {

    constructor(props) {
        super(props)
        this.state = {
            loan: { ...this.props.loan },
            selectOptions: [],
            id: "",
            name: ''
        };
    }

    async getOptions() {
        const res = await axios.get('http://localhost:9222/api/loans')
        const data = res.data

        const options = data.map(d => ({
            "value": d.customerId,
            "label": d.customerId
        }))
        
        this.setState({ selectOptions: options })

    }

    handleChange(e) {
        this.setState({ id: e.value, name: e.label })
    }

    render() {
        return (
            <>
                {
                    this.props.flag ?
                        <>
                            <div className='mt-3 mb-3'>
                                <ApplyLoanButton />&nbsp;&nbsp;
                                <CustomerLoansButton />&nbsp;&nbsp;
                            </div>
                            <div className='mt-3 mb-3'>
                                <Select options={this.state.selectOptions} onChange={this.handleChange.bind(this)} />   
                                <p>You have selected CustomerId: <strong>{this.state.name}</strong></p>
                            </div>
                            <LoanListComponentCustomerLoans loans={this.props.loans} onDelete={
                                (p, e) => {
                                    this.props.deleteLoan(p);
                                }
                            } />
                        </>
                        : <LoaderAnimation />
                }
            </>

        )
    }
    componentDidMount() {
        this.props.loadLoans();
        this.getOptions()
    }
}

function mapStateToProps(state, ownProps) {
    return {
        loans: state.loanReducer.loans,
        status: state.loanReducer.status,
        selectOptions: state.loanReducer.selectOptions,
        flag: state.loanReducer.flag
    };
}

function mapDispatchToProps(dispatch) {
    return {
        loadLoans: () => { dispatch(loanAction.loadLoans()); },
        deleteLoan: (loan) => { dispatch(loanAction.deleteLoan(loan)); }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerLoansContainer);