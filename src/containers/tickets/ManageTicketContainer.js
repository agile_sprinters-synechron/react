import React, { Component } from 'react';
import { connect } from 'react-redux';
import TicketFormComponent from '../../components/tickets/TicketFormComponent';
import getTicketById from '../../selectors/getTicketById';

import * as ticketActions from '../../actions/ticketActions';

class ManageTicketContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ticket: { ...this.props.ticket }
        };
        this.updateState = this.updateState.bind(this);
       this.saveTicket = this.saveTicket.bind(this);
    }

    updateState(e) {
        const field = e.target.name;
        let ticket = { ...this.state.ticket };
        ticket[field] = e.target.value;
        this.setState({ ticket: ticket });
    }

    saveTicket(e) {
        e.preventDefault();
            this.props.insertTicket(this.state.ticket);
    }

    render() {
        return (
            <div>
                <TicketFormComponent pageText={this.props.tText} ticket={this.state.ticket} 
                    onChange={this.updateState} onSave={this.saveTicket}/>
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    const ticketId = ownProps.match.params.id;

    let ticket = {
        id: "",
        customerid: "",
        email_address: "",
        message: "",
        phone: "",
        status: ""
    };

    if (ticketId && state.ticketReducer.tickets.length > 0) {
        ticket = getTicketById(state, ownProps);
    }

    var tText = "Create ticket" ;

    return {
        tText, ticket
    };
}

function mapDispatchToProps(dispatch) {
    return {
        insertTicket: (ticket) => { dispatch(ticketActions.insertTicket(ticket)); },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageTicketContainer);