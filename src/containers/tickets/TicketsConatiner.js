import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as ticketActions from '../../actions/ticketActions';
import LoaderAnimation from '../../components/common/LoaderAnimation';
import AddTicketButton from '../../components/tickets/AddTicketButton';
import TicketListComponent from '../../components/tickets/TicketListComponents';

class TicketsContainer extends Component {
    render() {
        return (
            <>
                {
                    this.props.flag ?
                        <>
                            <div className='mt-3 mb-3'>
                                <AddTicketButton />
                            </div>
                            <TicketListComponent tickets={this.props.tickets} onDelete={
                                (t, e) => {
                                    this.props.deleteTicket(t);
                                }
                            } />
                        </>
                        : <LoaderAnimation />
                }
            </>
        );
    }

    componentDidMount() {
       this.props.loadTickets();
    }
}

function mapStateToProps(state, ownProps) {
    return {
        tickets: state.ticketReducer.tickets,
        status: state.ticketReducer.status,
        flag: state.ticketReducer.flag
    };
}

function mapDispatchToProps(dispatch) {
    return {
        loadTickets: () => { dispatch(ticketActions.loadTickets()); }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(TicketsContainer);