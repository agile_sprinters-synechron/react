import React, { Component } from 'react';
import AccountService from '../../services/AccountService';
//import Table from "./Table";
import { NavLink } from 'react-router-dom';
//import DatePicker from '../accounts/DatePicker.css';

class BalanceComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
            balanceInfo: [],
            msg : ''
            // flag : "false"

        }

        this.changeIdHandler = this.changeIdHandler.bind(this);

        this.getBalance = this.getBalance.bind(this);
    }

    changeIdHandler = (event) => {
        this.setState({ id: event.target.value });
    }

    getBalance = (e) => {
        e.preventDefault()
        this.setState({msg : ''})
        this.setState({balanceInfo : []})
          if(this.state.id === '')
          {
            this.setState({msg : 'Please provide Account Id'})
            this.setState({ balanceInfo: [] })
            return 0
          }
          if(isNaN(this.state.id))
          {
            this.setState({msg : 'Please enter valid Account Id'})
            return 0
          }


        
        
        AccountService.getBalance(this.state.id).then((response) => {

            console.log(this.state.balanceInfo);
            this.setState({ balanceInfo: response.data })
            if(this.state.balanceInfo.length === 0)
                this.setState({msg : 'No record found for the entered Account Id'})
            else
                this.setState({msg : ''})

            console.log(this.state.balanceInfo);
        });
    }



    render() {
        return (
            <div>
                
                <div className='container'>
                    <div>
                        <NavLink className="nav-link d-flex flex-column align-items-center" to="/accounts">

                            <span>Back to Accounts Menu</span>
                        </NavLink>
                    </div>
                    <div className='row'>
                        <div className='card col-md-6 offset-md-3 offset-md-3'>
                            <h3 className='text-center'>Get Balance</h3>
                            <div className='card-body'>
                                <form>
                                    <div className='form-group'>
                                    Account ID <label className='mandatory'>*</label>
                                        <input placeholder='Account ID' className='form-control'
                                            value={this.state.id} onChange={this.changeIdHandler} />
                                    </div>
                                    <br/>

                                    <button className='btn btn-success' onClick={this.getBalance}>Get Balance</button>
                                    <br/>
                                    <table className='table table-stripped table-bordered' flag={this.state.flag}>
                                        <thead>
                                            <tr>
                                                <td>Id</td>
                                                <td>Customer ID</td>
                                                <td>Credit Score</td>
                                                <td>Customer Name</td>
                                                <td>Balance</td>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.balanceInfo.map(
                                                    bal =>

                                                        <tr key={bal.accountid}>
                                                            <td>{bal.accountid}</td>
                                                            <td>{bal.customerId}</td>
                                                            <td>{bal.creditScore}</td>
                                                            <td>{bal.customerName}</td>
                                                            <td>{bal.balance}</td>
                                                        </tr>
                                                )
                                            }
                                        </tbody>
                                    </table>
                                    <div>
                            <label className='errorLabel'>{this.state.msg}</label>
                        </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        );
    }
}

export default BalanceComponent;