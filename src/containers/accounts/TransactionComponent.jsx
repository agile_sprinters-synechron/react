import React, { Component } from 'react';
import AccountService from '../../services/AccountService';
//import Table from "./Table";
import { NavLink } from 'react-router-dom';
class TransactionComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
            amount: '',
            type: 'DEBIT',
            msg: '',
            success: ''
            // flag : "false"

        }

        this.changeIdHandler = this.changeIdHandler.bind(this);
        this.changeAmtHandler = this.changeAmtHandler.bind(this);
        this.changeDCHandler = this.changeDCHandler.bind(this);

        this.makeTransaction = this.makeTransaction.bind(this);
    }

    changeIdHandler = (event) => {
        this.setState({ id: event.target.value });
    }
    changeAmtHandler = (event) => {
        this.setState({ amount: event.target.value });
    }
    changeDCHandler = (event) => {
        this.setState({ type: event.target.value });
    }


    makeTransaction = (p) => {
        p.preventDefault();
        this.setState({ msg: '' })
        this.setState({ success: '' })
        if (this.state.id === '') {
            this.setState({ msg: 'Please provide Account Id' })
            this.setState({ balanceInfo: [] })
            return 0
        }
        if (isNaN(this.state.id)) {
            this.setState({ msg: 'Please enter valid Account Id' })
            return 0
        }

        if (this.state.amount === '') {
            this.setState({ msg: 'Please provide Amount' })
            this.setState({ balanceInfo: [] })
            return 0
        }
        if (isNaN(this.state.amount)) {
            this.setState({ msg: 'Please enter valid Amount' })
            return 0
        }



        let transaction = { id: this.state.id, amount: this.state.amount, type: this.state.type }

        

        AccountService.makeTransaction(transaction).then(res => {
            console.log(res)
            console.log("inside then")
            console.log(transaction);
            this.setState({ success: `${this.state.type} of RS/- ${this.state.amount} is successfull` });
        }, reason => {
            console.log(reason)
            console.log("inside reason")
            console.log(transaction)
            this.setState({ msg: `Account is not available for the entered Account ID` });
        });

        // ).else(res => {
        //     this.setState({ success: `Account is not available for this Account ID` });
        // })
    }

    render() {
        return (
            <div>
                <div className='container'>
                    <div>
                        <NavLink className="nav-link d-flex flex-column align-items-center" to="/accounts">

                            <span>Back to Accounts Menu</span>
                        </NavLink>
                    </div>
                    <div className='row'>
                        <div className='card col-md-6 offset-md-3 offset-md-3'>
                            <h3 className='text-center'>Make Transaction</h3>
                            <div className='card-body'>
                                <form>
                                    <div className='form-group'>
                                    Account ID <label className='mandatory'>*</label>
                                        <input placeholder='Account ID' className='form-control'
                                            value={this.state.id} onChange={this.changeIdHandler} />
                                    </div>
                                    <br />
                                    <div className='form-group'>
                                    Amount <label className='mandatory'>*</label>
                                        <input placeholder='Amount' className='form-control'
                                            value={this.state.amount} onChange={this.changeAmtHandler} />
                                    </div>
                                    <br />
                                    {/* <div className='form-group'>
                                        <label>DEBIT/CREDIT</label>
                                        <input placeholder='DEBIT/CREDIT' className='form-control'
                                            value={this.state.type} onChange={this.changeDCHandler} />
                                    </div> */}
                                    <div>
                                       
                                         Transaction <label className='mandatory'>* </label> &nbsp;
                                            <select value={this.state.value} onChange={this.changeDCHandler}>
                                                <option value="DEBIT">DEBIT</option>
                                                <option value="CREDIT">CREDIT</option>
                                            </select>
                                       
                                    </div>
                                    <br />
                                    <button className='btn btn-success' onClick={this.makeTransaction}>Submit</button>
                                    <div>
                                        <label className='errorLabel'>{this.state.msg}</label>
                                        <label className='successLabel'>{this.state.success}</label>
                                    </div>


                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        );
    }
}

export default TransactionComponent;