import React, { Component } from 'react';
import AccountService from '../../services/AccountService';
import DatePicker from '../accounts/DatePicker.css';
import { DatePickerComponent } from "@syncfusion/ej2-react-calendars";
import { NavLink } from 'react-router-dom';

//import Table from "./Table";

class StatementComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
            bal: '',
            accountId: '',
            transactionType: '',
            amount: '',
            time: '',
            msg: '',

            // dateValue: Date = new Date(new Date().getFullYear(), new Date().getMonth(), 14),
            // startDate: Date : '',//new Date("04/02/2008"),
            // enddate: Date : '',//new Date("04/02/2008"),
            startDate: new Date(new Date().getFullYear(), new Date().getUTCMonth() - 12, 1),
            enddate: new Date(new Date().getFullYear(), new Date().getUTCMonth() + 12, 1),
            statementInfo: []
            // flag : "false"

        }

        this.changeIdHandler = this.changeIdHandler.bind(this);
        this.changeStartDateHandler = this.changeStartDateHandler.bind(this);
        this.changeEndDateHandler = this.changeEndDateHandler.bind(this);

        this.getRecords = this.getRecords.bind(this);
    }

    changeIdHandler = (event) => {
        this.setState({ id: event.target.value });
    }
    changeStartDateHandler = (event) => {
        this.setState({ startDate: event.target.value });
    }
    changeEndDateHandler = (event) => {
        this.setState({ enddate: event.target.value });
    }

    getRecords = (e) => {
        e.preventDefault()
        this.setState({ statementInfo: [] })
        this.setState({ msg: '' })

        if (this.state.id === '') {
            this.setState({ msg: 'Please provide Account Id' })
            this.setState({ balanceInfo: [] })
            return 0
        }
        if (isNaN(this.state.id)) {
            this.setState({ msg: 'Please enter valid Account Id' })
            return 0
        }

        AccountService.getStatements(this.state.id, this.state.startDate, this.state.enddate).then((response) => {

            console.log(this.state.statementInfo);
            this.setState({ msg: '' })
            this.setState({ statementInfo: response.data })
            console.log(this.state.statementInfo.length)
            if (this.state.statementInfo.length == 0)
                this.setState({ msg: 'No records found' })
            else
                this.setState({ msg: '' })
            console.log(this.state.statementInfo);
            this.setState({startDate: new Date(new Date().getFullYear(), new Date().getUTCMonth() - 12, 1)})
            this.setState({enddate: new Date(new Date().getFullYear(), new Date().getUTCMonth() + 12, 1)})
        },
         reason => {
            console.log(reason)
            console.log("inside reason")
           // console.log(transaction)
            this.setState({ msg: `Account is not available for the entered Account ID` });
        });
    }

    render() {
        return (
            <div>
                <div className='container'>
                    <div>
                        <NavLink className="nav-link d-flex flex-column align-items-center" to="/accounts">

                            <span>Back to Accounts Menu</span>
                        </NavLink>
                    </div>
                    <div className='row'>
                        <div className='card col-md-6 offset-md-3 offset-md-3'>
                            <h3 className='text-center'>Get Statements</h3>
                            <div className='card-body'>
                                <form>
                                    <div className='form-group'>
                                    Account ID <label className='mandatory'>*</label>
                                        <input placeholder='Account Id' className='form-control'
                                            value={this.state.id} onChange={this.changeIdHandler} />
                                    </div>
                                    <div className='form-group'>
                                        <label>From</label>
                                        <div>
                                            <DatePickerComponent placeholder="From"
                                                value={this.state.dateValue}
                                                min={this.state.startDate}
                                                max={this.state.enddate}
                                                format="dd-MM-yyyy" onChange={this.changeStartDateHandler}
                                            // Uncomment below properties to show month picker. Note that, range restiction (min and max properties) should be removed for this case. 
                                            // start="Year"
                                            // depth="Year"
                                            ></DatePickerComponent>
                                        </div>
                                    </div>
                                    <div className='form-group'>
                                        <label>To</label>
                                        <div>
                                            <DatePickerComponent placeholder="To"
                                                value={this.state.dateValue}
                                                min={this.state.startDate}
                                                max={this.state.enddate}
                                                format="dd-MM-yyyy" onChange={this.changeEndDateHandler}
                                            // Uncomment below properties to show month picker. Note that, range restiction (min and max properties) should be removed for this case. 
                                            // start="Year"
                                            // depth="Year"
                                            ></DatePickerComponent>
                                        </div>
                                    </div>
                                    <button className='btn btn-success' onClick={this.getRecords}>Submit</button>
                                    {/* <table className='table table-stripped table-bordered' flag={this.state.flag}>
                                        <thead>
                                            <tr>
                                                <td>Statement ID</td>
                                                <td>Account ID</td>
                                                <td>Transaction</td>
                                                <td>Amount</td>
                                                <td>Time</td>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.statementInfo.map(
                                                    bal =>

                                                        <tr key={bal.sid}>
                                                            <td>{bal.sid}</td>
                                                            <td>{bal.accountId}</td>
                                                            <td>{bal.transactionType}</td>
                                                            <td>{bal.amount}</td>
                                                            <td>{bal.time}</td>
                                                        </tr>
                                                )
                                            }
                                        </tbody>
                                    </table> */}


                                </form>
                            </div>
                        </div>
                        <table className='table table-stripped table-bordered' flag={this.state.flag}>
                            <thead>
                                <tr>
                                    <td>Statement ID</td>
                                    <td>Account ID</td>
                                    <td>Transaction</td>
                                    <td>Amount</td>
                                    <td>Time</td>

                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.statementInfo.map(
                                        bal =>

                                            <tr key={bal.sid}>
                                                <td>{bal.sid}</td>
                                                <td>{bal.accountId}</td>
                                                <td>{bal.transactionType}</td>
                                                <td>{bal.amount}</td>
                                                <td>{bal.time}</td>
                                            </tr>
                                    )
                                }
                            </tbody>
                        </table>
                        <div>
                            <label className='errorLabel'>{this.state.msg}</label>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default StatementComponent;