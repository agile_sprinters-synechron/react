import React, { Component } from 'react';

import AccountsMenu from './AccountsMenu';

class AccountsContainer extends Component {
    render() {
        return (
            <>
              <h2>Accounts Service</h2>
              <hr/>
              <AccountsMenu/>
            </>
        );
    }
}



export default AccountsContainer