import React, { Component } from 'react';
import { Link } from 'react-router-dom';
class AccountsMenu extends Component {
    render() {
        return (
            <div>
              <Link className="text-info" to={"balance/"}>Balance Info</Link><br/><br/>
              <Link className="text-info" to={"transaction/"}>Credit/Debit</Link><br/><br/>
              {/* <Link className="text-info" to={"debit/"}>Debit</Link><br/><br/> */}
              <Link className="text-info" to={"statements/"}>Statement</Link>
            </div>
        );
    }
}

export default AccountsMenu;