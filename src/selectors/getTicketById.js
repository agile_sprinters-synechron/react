// npm i reselect

import { createSelector } from 'reselect';

const getTickets = (state, props) => state.ticketReducer.tickets;
const getId = (state, props) => props.match.params.id;

const getTicketsById = createSelector(getTickets, getId, function (tickets, id) {
    const ticket = tickets.find(t => t.id === parseInt(id));
    return ticket;
});

export default getTicketsById;