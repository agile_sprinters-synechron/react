// npm i reselect

import { createSelector } from 'reselect';

const getLoans = (state, props) => state.loanReducer.loans;
const getLoanId = (state, props) => props.match.params.loanId;

const getLoanById = createSelector(getLoans, getLoanId, function (loans, loanId) {
    const loan = loans.find(p => p.loanId === parseInt(loanId));
    return loan;
});

export default getLoanById;